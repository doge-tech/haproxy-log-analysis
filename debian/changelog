haproxy-log-analysis (6.0.0~a4-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version 6.0.0~a4
  * d/watch: Use release tarballs instead of tags.
  * d/control: Use pybuild with pyproject.
  * d/rules: Update help2man command for new src directory.
  * Mark "help" autopkgtest as superficial.
  * Bump Standards-Version to 4.7.0, no changes needed.
  * README.rst: Remove remote images for privacy.
  * Fix timezone for tests/test_commands.py. The hardcoded expected datetime
    values in the tests are timezone-specific, and would cause tests to
    fail if run in an environment more than one hour away from UTC.

 -- Dale Richards <dale@dalerichards.net>  Mon, 10 Jun 2024 22:50:36 +0100

haproxy-log-analysis (5.1.0-2) unstable; urgency=medium

  * Team upload.
  * Declare Rules-Requires-Root: no.
  * Correct autopkgtests (whoops!)

 -- Stefano Rivera <stefanor@debian.org>  Sun, 19 Nov 2023 17:17:38 +0200

haproxy-log-analysis (5.1.0-1) unstable; urgency=medium

  * Team upload.

  [ Dale Richards ]
  * New upstream version 5.1.0 (Closes: #1038111, #1040286).
  * Review and update to Debian Policy version 4.6.2.
    - Increase build verbosity.
  * Run tests with pytest.

  [ Stefano Rivera ]
  * Update watch file to account for _ in the filename.
  * Move source package and binary to utils section (Closes: #825599).
  * Add autopkgtests.
  * Update copyright.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 19 Nov 2023 17:02:20 +0200

haproxy-log-analysis (2.0~b0-4) unstable; urgency=medium

  * Set upstream metadata fields: Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 28 May 2022 01:09:38 +0100

haproxy-log-analysis (2.0~b0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Mon, 02 May 2022 23:40:49 -0400

haproxy-log-analysis (2.0~b0-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Use DEB_VERSION_UPSTREAM instead of dpkg-parsechangelog
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Mon, 05 Aug 2019 09:48:58 +0200

haproxy-log-analysis (2.0~b0-1) unstable; urgency=low

  * Initial release (Closes: #821339)

 -- Christopher Baines <mail@cbaines.net>  Mon, 09 May 2016 12:13:52 +0000
